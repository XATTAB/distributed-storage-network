﻿using System.Collections.Generic;
using System.ServiceModel;
using DSN.Library;

namespace DSN.Contracts
{
    [ServiceContract]
    public interface IContractServer
    {
        [OperationContract]
        ServerState GetState();

        [OperationContract]
        List<FileInfo> GetListFiles();

        [OperationContract]
        IEnumerable<byte> GetBytesArray(string file, long position, long size);
    }
}
