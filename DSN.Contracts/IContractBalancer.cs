﻿using System.Collections.Generic;
using System.ServiceModel;
using DSN.Library;

namespace DSN.Contracts
{
    [ServiceContract]
    public interface IContractBalancer
    {
        [OperationContract]
        List<FileInfo> GetListFiles();
    }
}
