﻿using System.Collections.Generic;

namespace DSN.Client.Models
{
    public class BlockBytes
    {
        public IEnumerable<byte> Bytes { get; set; }
        public long Position { get; set; }
    }
}