namespace DSN.Client.Models
{
    public class DataGridModel
    {
        public string NameFile { get; set; }
        public int CountServer { get; set; }
        public string Size { get; set; }
        public string MD5 { get; set; }
    }
}