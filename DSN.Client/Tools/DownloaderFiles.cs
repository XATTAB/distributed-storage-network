﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using DSN.Client.Models;
using DSN.Client.WCF;
using DSN.Contracts;
using DSN.Library;
using FileInfo = DSN.Library.FileInfo;

namespace DSN.Client.Tools
{
    public class DownloaderFiles
    {
        private readonly Stopwatch _stopwatch = new Stopwatch();
        private readonly ClientToServer _clientToServer;
        private object _threadLock = new object();
        private readonly Queue<long> _queue;
        private readonly List<byte> _files;
        private readonly List<BlockBytes> _blockByteses;

        private readonly long _lastBlockPosition;
        private readonly long _remainder;
        private readonly long _maxSize;

        private readonly FileInfo _fileInfo;

        private readonly ProgressBar _progress;

        public DownloaderFiles(FileInfo fileInfo, ProgressBar progress)
        {
            _clientToServer = ClientToServer.Instance();

            var listIp = fileInfo.Files.Select(x => x.Ip);
            var ipList = listIp as IList<string> ?? listIp.ToList();
            _clientToServer.AddIp(ipList);

            if(ipList.Count == 0) throw new ArgumentException("Нет доступных серверов!");

            //long maxSize = fileInfo.Size / ipList.Count / 4;
            long maxSize = SelectedMaxSize(fileInfo.Size, ipList.Count);
            

            _progress = progress;

            _progress.Dispatcher.BeginInvoke(new Action(delegate {_progress.Value = 0;}));    
            
            _blockByteses = new List<BlockBytes>();

            _fileInfo = fileInfo;

            _queue = new Queue<long>();

            _files = new List<byte>();

            long countBlock;
            if (maxSize < fileInfo.Size)
            {
                countBlock = _fileInfo.Size/maxSize;
                _remainder = _fileInfo.Size - countBlock * maxSize;
                _maxSize = maxSize;
            }
            else
            {
                countBlock = 1;
                _remainder = 0;
                _maxSize = fileInfo.Size;
            }

            for (int i = 0; i < countBlock; i++)
            {
                _queue.Enqueue(i * _maxSize);
            }

            _lastBlockPosition = _queue.Count * _maxSize;
        }

        private long SelectedMaxSize(long fileSize, int countServer)
        {
            long oneKb = 1024;
            long oneMb = oneKb*1024;

            long maxSize = 0;

            if (fileSize < oneKb)
                maxSize = fileSize/countServer;

            if (fileSize > oneKb && fileSize < oneMb)
                maxSize = fileSize / countServer / 2;

            if (fileSize > oneMb)
                maxSize = fileSize / countServer / 4;

            if(fileSize > 200 * oneMb)
                maxSize = fileSize / countServer / 50;

            if (maxSize == 0) maxSize = 1;

            if (maxSize > int.MaxValue) maxSize = int.MaxValue - 1;

            return maxSize;
        }

        private void TranslateByte()
        {
            _blockByteses.Sort((x, y) => x.Position.CompareTo(y.Position));

            foreach (var enumByte in _blockByteses)
            {
                foreach (var b in enumByte.Bytes)
                {
                    _files.Add(b);
                }
            }
        }

        private void ParallelLoading(ChannelFactory<IContractServer> factory)
        {
            var chanel = factory.CreateChannel();

            var fullName = string.Empty;

            var fileNameAndIp = _fileInfo.Files.FirstOrDefault(x => x.Ip == factory.Endpoint.Address.Uri.Host);

            if (fileNameAndIp != null)
            {
                fullName = fileNameAndIp.FullName;
            }

            while (ContinueDownload())
            {
                var position = GetPosition();

                if(position == -1) break;

                var bufferByte = chanel.GetBytesArray(fullName, position, _maxSize);

                _blockByteses.Add(new BlockBytes
                {
                    Bytes = bufferByte,
                    Position = position
                });
            }

            //MessageBox.Show(Thread.CurrentThread.Name);
        }

        public void Start()
        {
            if(_clientToServer.GetChannelFactories().Count == 0) return;

            // Замер времени загрузки
            _stopwatch.Start();

            Parallel.ForEach(_clientToServer.GetChannelFactories(), ParallelLoading); // Параллельный вариант
            //foreach (var channelFactory in clientToServer.GetChannelFactories()) {ParallelLoading(channelFactory);} // Последовательный вариант
            //ParallelLoading(clientToServer.GetChannelFactories().FirstOrDefault()); // Вариант загрузки с первого сервера
         
            _stopwatch.Stop();

            // Если есть байты выходящие за целый блок
            if (_remainder > 0)
            {
                var firstOrDefault = _clientToServer.GetChannelFactories().FirstOrDefault();

                var chanel = firstOrDefault.CreateChannel();

                var fileNameAndIp = _fileInfo.Files.FirstOrDefault(x => x.Ip == firstOrDefault.Endpoint.Address.Uri.Host);
                if (fileNameAndIp != null)
                {
                    var fullName =
                        fileNameAndIp.FullName;

                    var bufferByte = chanel.GetBytesArray(fullName, _lastBlockPosition, _remainder);

                    _blockByteses.Add(new BlockBytes
                    {
                        Bytes = bufferByte,
                        Position = _lastBlockPosition
                    });
                }
            }

            TranslateByte();

            SaveFile();
        }

        private long GetPosition()
        {
            lock (_threadLock)
            {
                if (_queue.Count != 0)
                {
                    var percent = 100 / _queue.Count;

                    _progress.Dispatcher.BeginInvoke(new Action(delegate { _progress.Value = percent; }));

                    return _queue.Dequeue();
                }
                else
                {
                    return -1;
                }
            }
           


            //return _queue.Dequeue();
        }

        private bool ContinueDownload()
        {
           //if (_queue.Count != 0)
           // {
           //     var percent = 100/_queue.Count;

           //     _progress.Dispatcher.BeginInvoke(new Action(delegate {_progress.Value = percent; }));
           // }

            return _queue.Count != 0;
        }

        private void SaveFile()
        {
            var catalogName = @"Downloads\";

            Directory.CreateDirectory(catalogName);

            File.WriteAllBytes(catalogName + _fileInfo.Name, _files.ToArray());

            _progress.Dispatcher.BeginInvoke(new Action(delegate {_progress.Value = 100;}));

            // Проверка успешной загрузки, сравнение хэш сумм
            if (CustomerMd5.ComputeMd5Checksum(catalogName + _fileInfo.Name) == _fileInfo.Hash)
                MessageBox.Show("Загрузка успешно завершена!" + Environment.NewLine + "Время загрузки: " + _stopwatch.Elapsed, "Внимание!", MessageBoxButton.OK, MessageBoxImage.Information);
            else
                MessageBox.Show("Произошел сбой загруки!", "Внимание!", MessageBoxButton.OK, MessageBoxImage.Warning);
            
        }
    }
}