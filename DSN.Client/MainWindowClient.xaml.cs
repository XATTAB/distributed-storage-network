﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows;
using DSN.Client.Models;
using DSN.Client.Tools;
using DSN.Client.WCF;
using DSN.Library;
using FileInfo = DSN.Library.FileInfo;

namespace DSN.Client
{
    public partial class MainWindowClient
    {
        private readonly ClientToBalancer _clientToBalancer;
        private readonly List<FileInfo> _dataFiles;

        private Thread _threadDownload;

        public MainWindowClient()
        {
            // Пустой поток для корректной работы отмены
            _threadDownload = new Thread(() => {});

            InitializeComponent();

            _dataFiles = new List<FileInfo>();

            try
            {
                _clientToBalancer = ClientToBalancer.Instance();
            }
            catch (Exception ex)
            {
                Notification.ToReport(ex);

                Close();
            }
        }

        private void getListFileButton_Click(object sender, RoutedEventArgs e)
        {
            var tempList = new List<DataGridModel>();
            tempList.Clear();
            
            DataGrid.Items.Refresh();

            if (!_clientToBalancer.IsWorking())
            {
                MessageBox.Show("Сервер балансировщик, временно не доступен!", "Внимание!", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;   
            }

            var listFile = _clientToBalancer.GetFileInfo();

            if (listFile == null || listFile.Count == 0) return;

            foreach (var fileinformation in listFile)
            {
                var data = new DataGridModel
                {
                    NameFile = fileinformation.Name,
                    CountServer = fileinformation.Files.Count,
                    MD5 = fileinformation.Hash,
                    Size = $"{fileinformation.Size/1024}.{fileinformation.Size%1024} KB"
                };

                _dataFiles.Add(fileinformation);

                tempList.Add(data);
            }

            DataGrid.ItemsSource = tempList;
        }

        private void Loadingbutton1_Click(object sender, RoutedEventArgs e)
        {
            Loadingbutton.IsEnabled = false;
            CancleButton.IsEnabled = true;

            var selectedFile = _dataFiles.FirstOrDefault(x => x.Name == ((DataGridModel)DataGrid.SelectedCells[0].Item).NameFile);

            if(selectedFile == null) return;

            _threadDownload = new Thread(() => { new DownloaderFiles(selectedFile, ProgressBar).Start(); });

            _threadDownload.Start();
        }

        private void DataGrid_SelectedCellsChanged(object sender, System.Windows.Controls.SelectedCellsChangedEventArgs e)
        {
            if(DataGrid.SelectedIndex == -1) return;

            if(!_threadDownload.IsAlive)
                Loadingbutton.IsEnabled = true;
        }

        private void CancleButton_Click(object sender, RoutedEventArgs e)
        {
            _threadDownload.Abort();

            Loadingbutton.IsEnabled = true;
            CancleButton.IsEnabled = false;

            ProgressBar.Value = 0;
        }

        private void ProgressBar_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if(ProgressBar.Value == 100)
                CancleButton.IsEnabled = false;
        }
    }
}
