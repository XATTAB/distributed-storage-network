﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Description;
using DSN.Contracts;
using DSN.Library;

namespace DSN.Client.WCF
{
    public class ClientToBalancer
    {
        static ClientToBalancer _uniqueInstance;

        private readonly IContractBalancer _channel;

        private const string Ip = "40.127.195.139";

        private Uri address;

        private ClientToBalancer()
        {
            address = new Uri("http://" + Ip + ":4000/Balancer");
            var binding = new BasicHttpBinding();

            var factory = new ChannelFactory<IContractBalancer>(binding, new EndpointAddress(address));

            _channel = factory.CreateChannel();
        }
        
        public static ClientToBalancer Instance()
        {
            if (_uniqueInstance == null)
                _uniqueInstance = new ClientToBalancer();

            return _uniqueInstance;
        }

        private IContractBalancer GetChanel()
        {
            return _channel;
        }

        public bool IsWorking()
        {
            try
            {
                MetadataExchangeClient metaTransfer = new MetadataExchangeClient(address, MetadataExchangeClientMode.HttpGet);
                metaTransfer.OperationTimeout = new TimeSpan(0, 0, 5);
                metaTransfer.GetMetadata();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<FileInfo> GetFileInfo()
        {
            return _channel.GetListFiles();
        }
    }
}