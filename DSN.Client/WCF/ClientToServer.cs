﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Description;
using DSN.Contracts;

namespace DSN.Client.WCF
{
    public class ClientToServer
    {
        static ClientToServer _uniqueInstance;

        private readonly List<Uri> _listUri;
        private readonly BasicHttpBinding _binding;
        private readonly List<IContractServer> _listChanel;
        private readonly List<ChannelFactory<IContractServer>> _channelFactories;

        private ClientToServer()
        {
            _listUri = new List<Uri>();
            _binding = new BasicHttpBinding();

            // TODO Проверить
            _binding.MessageEncoding = WSMessageEncoding.Mtom;
            _binding.MaxReceivedMessageSize = Int32.MaxValue;
            _binding.TransferMode = TransferMode.Streamed;
            _binding.ReceiveTimeout = new TimeSpan(0, 2, 0);
            _binding.SendTimeout = new TimeSpan(0, 2, 0);
            _binding.CloseTimeout = new TimeSpan(0, 10, 0);
            _binding.MaxBufferSize = Int32.MaxValue;
            

            _listChanel = new List<IContractServer>();
            _channelFactories = new List<ChannelFactory<IContractServer>>();

            //_binding.MaxReceivedMessageSize = Int32.MaxValue;
        }

        // Используется паттерн Singleton
        public static ClientToServer Instance()
        {
            if (_uniqueInstance == null)
                _uniqueInstance = new ClientToServer();

            return _uniqueInstance;
        }

        public long GetMaxSizeBuffer()
        {
            return _binding.MaxReceivedMessageSize;
        }

        public void AddIp(IEnumerable<string> ipList)
        {
            _listUri.Clear();
            _listChanel.Clear();
            _channelFactories.Clear();

            foreach (var ip in ipList)
            {
                _listUri.Add(new Uri("http://" + ip + ":4000/ServiceGetState"));
            }
        }

        public List<ChannelFactory<IContractServer>> GetChannelFactories()
        {
            foreach (var uri in _listUri)
            {
                if(CheckAvailabilityService(uri.ToString()))
                    _channelFactories.Add(new ChannelFactory<IContractServer>(_binding, new EndpointAddress(uri)));
            }

            return _channelFactories;
        }

        private bool CheckAvailabilityService(string address)
        {
            try
            {
                MetadataExchangeClient metaTransfer = new MetadataExchangeClient(new Uri(address), MetadataExchangeClientMode.HttpGet);
                metaTransfer.GetMetadata();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<IContractServer> GetListChanel()
        {
            return _listChanel;
        }


    }
}
