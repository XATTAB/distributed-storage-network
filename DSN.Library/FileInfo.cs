﻿using System.Collections.Generic;
using System.ServiceModel;

namespace DSN.Library
{
    [MessageContract]
    public class FileInfo
    {
        public FileInfo()
        {
            Files = new List<FileNameAndIp>();
        }

        [MessageBodyMember]
        public string Name { get; set; }
        [MessageBodyMember]
        public long Size { get; set; }
        [MessageBodyMember]
        public string Hash { get; set; }
        [MessageBodyMember]
        public string FullName { get; set; }
        [MessageBodyMember]
        public List<FileNameAndIp> Files { get; set; }
    }

    [MessageContract]
    public class FileNameAndIp
    {
        [MessageBodyMember]
        public string FullName { get; set; }
        [MessageBodyMember]
        public string Ip { get; set; }
    }
}
