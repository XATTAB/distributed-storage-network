﻿using System;
using System.Windows;

namespace DSN.Library
{
    public static class Notification
    {
        public static void ToReport(Exception ex)
        {
            MessageBox.Show(ex.Message, "Внимание!", MessageBoxButton.OK, MessageBoxImage.Warning);
        }
    }
}