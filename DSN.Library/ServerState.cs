﻿namespace DSN.Library
{
    public class ServerState
    {
        public string StateCpu { get; set; }

        public string StateRam { get; set; }
    }
}
