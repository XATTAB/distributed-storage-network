﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Threading;
using DSN.Balancer.Models;
using DSN.Balancer.WCF;

namespace DSN.Balancer
{
    public partial class MainWindowBalancer
    {
        // Экземпляр класса сервера балансировщика
        private readonly BalancerServer _balancerServer;

        // Экземпляр класса подключения балансировщика к серверам
        private readonly BalancerToServer _balancerToServer;

        // Таймер опроса серверов
        private readonly DispatcherTimer _timer;

        // Список данных для отображения в DataGrid
        private readonly List<DataGridModel> _listData;

        public MainWindowBalancer()
        {
            InitializeComponent();
            
            _listData = new List<DataGridModel>();

            _timer = new DispatcherTimer {Interval = new TimeSpan(0, 1, 0)};
            _timer.Tick += (sender, args) => Refrash_Info(sender, null);
            
            _balancerServer = BalancerServer.Instance();
            _balancerToServer = BalancerToServer.Instance();

            DataGrid.ItemsSource = _listData;
        }

        private void buttonStart_Click(object sender, RoutedEventArgs e)
        {
            if (_balancerServer.Start())
            {
                TextBoxStatus.Text = "Сервер запущен: " + DateTime.Now.ToShortTimeString();
                ButtonStop.IsEnabled = true;
                ButtonStart.IsEnabled = false;

                _timer.Start();

                Refrash_Info(null, null);
            }
            else
            {
                TextBoxStatus.Text = "Ошибка, сервер не запущен!";
                ButtonStop.IsEnabled = false;
                ButtonStart.IsEnabled = true;
            }
        }

        private void buttonStop_Click(object sender, RoutedEventArgs e)
        {
            if (_balancerServer.Stop())
            {
                TextBoxStatus.Text = "Сервер остановлен: " + DateTime.Now.ToShortTimeString() + Environment.NewLine;
                ButtonStart.IsEnabled = true;
                ButtonStop.IsEnabled = false;

                _timer.Stop();
            }
            else
            {
                TextBoxStatus.Text = "Ошибка, сервер не запущен!" + Environment.NewLine;
                ButtonStop.IsEnabled = true;
                ButtonStart.IsEnabled = false;
            }
        }

        private void Refrash_Info(object sender, RoutedEventArgs e)
        {
            _balancerToServer.Start();

            var listFiles = _balancerToServer.GetListFileInfos();
            BalancerService.AddFile(listFiles);

            var listIp = _balancerToServer.GetActiveHosts();
            var listState = _balancerToServer.GetStateServers();

            _listData.Clear();

            for (var i = 0; i < listIp.Count; i++)
            {
                _listData.Add(new DataGridModel
                {
                    Ip = listIp[i],
                    Cpu = listState[i].StateCpu + " %",
                    Ram = listState[i].StateRam + " %"
                });
            }

            DataGrid.Items.Refresh();
        }
    }
}
