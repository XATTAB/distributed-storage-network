﻿namespace DSN.Balancer.Models
{
    public class DataGridModel
    {
        public string Ip { get; set; }
        public string Cpu { get; set; }
        public string Ram { get; set; }
    }
}