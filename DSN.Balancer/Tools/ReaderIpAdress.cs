﻿using System;
using System.Collections.Generic;
using System.Xml;
using DSN.Library;

namespace DSN.Balancer.Tools
{
    public class ReaderIpAdress
    {
        private XmlTextReader _reader;

        public List<string> GetListIp()
        {
            _reader = new XmlTextReader("serverAddresses.xml");

            var list = new List<string>();
            
            try
            {
                while (_reader.Read())
                {
                    if(_reader.NodeType == XmlNodeType.Element && _reader.Name == "ip")
                        list.Add(_reader.ReadElementString());
                }        
            }
            catch (Exception ex)
            {
                Notification.ToReport(ex);

                return null;
            }

            return list;
        }
    }
}