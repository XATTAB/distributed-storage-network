﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using DSN.Contracts;
using DSN.Library;

namespace DSN.Balancer.WCF
{
    public class BalancerServer
    {
        private static BalancerServer _uniqueInstance;
        private readonly BasicHttpBinding _binding;
        private readonly ServiceMetadataBehavior _behavior;
        private readonly Uri _address;
        private ServiceHost _service;

        private BalancerServer()
        {
            _address = new Uri("http://localhost:4000/Balancer");
            _binding = new BasicHttpBinding();
            _behavior = new ServiceMetadataBehavior();
        }

        /// <summary>
        /// Получаем экземпляр класса BalancerServer, используется паттерн одиночка.
        /// </summary>
        /// <returns></returns>
        public static BalancerServer Instance()
        {
            if (_uniqueInstance == null)
                _uniqueInstance = new BalancerServer();

            return _uniqueInstance;
        }

        /// <summary>
        /// Запуск сервера.
        /// </summary>
        /// <returns>Успешно ли запущен сервер.</returns>
        public bool Start()
        {
            try
            {
                if (_service == null)
                {
                    _service = new ServiceHost(typeof(BalancerService), _address);
                    _service.AddServiceEndpoint(typeof(IContractBalancer), _binding, "");
                    _behavior.HttpGetEnabled = true;
                    _service.Description.Behaviors.Add(_behavior);
                    _service.AddServiceEndpoint(typeof(IMetadataExchange),
                        MetadataExchangeBindings.CreateMexHttpBinding(), _address + "mex");

                    _service.Open();
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Notification.ToReport(ex);
                _service = null;

                return false;
            }
        }

        /// <summary>
        /// Остановка сервера.
        /// </summary>
        /// <returns>Успешно ли остановлен сервер.</returns>
        public bool Stop()
        {
            try
            {
                if (_service != null)
                {
                    _service.Close();
                    _service = null;
                }
                return true;
            }
            catch (Exception ex)
            {
                Notification.ToReport(ex);
                return false;
            }
        }
    }
}
