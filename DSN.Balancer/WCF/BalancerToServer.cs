﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using DSN.Balancer.Tools;
using DSN.Contracts;
using DSN.Library;

namespace DSN.Balancer.WCF
{
    public class BalancerToServer
    {
        private static BalancerToServer _uniqueInstance;

        private const string Port = ":4000";
        private const string ServiceName = Port + "/ServiceGetState";
        private const string PrefixName = "dsn";

        private readonly List<Uri> _listUri;
        private readonly BasicHttpBinding _binding;
        private readonly List<IContractServer> _listChanel;
        private readonly List<ChannelFactory<IContractServer>> _listChannelFactories;

        private BalancerToServer()
        {
            _listUri = new List<Uri>();
            _listChanel = new List<IContractServer>();
            _listChannelFactories = new List<ChannelFactory<IContractServer>>();

            _binding = new BasicHttpBinding
            {
                MessageEncoding = WSMessageEncoding.Mtom,
                MaxReceivedMessageSize = Int32.MaxValue,
                TransferMode = TransferMode.Streamed,
                CloseTimeout = new TimeSpan(0, 10, 0),
                MaxBufferSize = Int32.MaxValue
            };

            ReadIpOfXml();
        }

        /// <summary>
        /// Читать список IP серверов, из XML файла.
        /// </summary>
        private void ReadIpOfXml()
        {
            var readerIp = new ReaderIpAdress();

            var listIp = readerIp.GetListIp();

            if (listIp == null) return;

            foreach (var ip in listIp){ AddIp(ip); }
        }

        /// <summary>
        /// Возвращает экземпляр класса BalancerToServer.
        /// </summary>
        /// <returns></returns>
        public static BalancerToServer Instance()
        {
            if (_uniqueInstance == null)
                _uniqueInstance = new BalancerToServer();

            return _uniqueInstance;
        }

        /// <summary>
        /// Проверка на отсутствие повторяющихся IP адресов.
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        private bool ChekIpOnContains(string ip)
        {
            return _listUri.Any(uri => uri.Host == ip);
        }

        /// <summary>
        /// Добавить IP адрес в список.
        /// </summary>
        /// <param name="ip"></param>
        private void AddIp(string ip)
        {
            if (!ChekIpOnContains(ip))
                _listUri.Add(new Uri("http://" + ip + ServiceName));
        }

        /// <summary>
        /// Создаем фабрики и каналы для работы серверами файлов.
        /// </summary>
        public void Start()
        {
            _listChanel.Clear();
            _listChannelFactories.Clear();

            foreach (Uri uri in _listUri)
            {
                try
                {
                    // Если сервис не доступен пропускаем его
                    if (CheckAvailabilityService(uri.ToString()))
                    {
                        var factory = new ChannelFactory<IContractServer>(_binding, new EndpointAddress(uri));
                        _listChannelFactories.Add(factory);
                        _listChanel.Add(factory.CreateChannel());
                    }
                }
                catch (Exception ex)
                {
                    Notification.ToReport(ex);
                }
            }
        }

        /// <summary>
        /// Получить список активных сервисов.
        /// </summary>
        /// <returns></returns>
        public List<string> GetActiveHosts()
        {
            Start();

            return _listChannelFactories.Select(channelFactory => channelFactory.Endpoint.Address.Uri.Host + Port).ToList();
        }
        
        /// <summary>
        /// Проверка доступности сервиса на сервере.
        /// </summary>
        /// <param name="address">IP адрес сервиса</param>
        /// <returns></returns>
        private bool CheckAvailabilityService(string address)
        {
            try
            {
                MetadataExchangeClient metaTransfer = new MetadataExchangeClient(new Uri(address), MetadataExchangeClientMode.HttpGet);

                // Получаем метта данные о сервисе, в случае если сервер не доступен получаем исключение
                metaTransfer.GetMetadata();

                return true;
            }
            catch (Exception) { return false; }
        }

        /// <summary>
        /// Получить список состояний серверов.
        /// </summary>
        /// <returns></returns>
        public List<ServerState> GetStateServers()
        {
            return _listChanel.Select(serverContract => serverContract.GetState()).ToList();
        } 

        /// <summary>
        /// Получить сгруппированный список файлов с адресами серверов и полным путем файла.
        /// </summary>
        /// <returns></returns>
        public List<FileInfo> GetListFileInfos()
        {
            //UpdateChanel();

            var list = new List<FileInfo>();

            foreach (var channelFactories in _listChannelFactories)
            {
                foreach (var listFile in channelFactories.CreateChannel().GetListFiles())
                {
                    var gottenFile = list.FirstOrDefault(x => x.Name == listFile.Name && x.Hash == listFile.Hash);

                    if (gottenFile == null)
                    {
                        var file = new FileInfo
                        {
                            Name = listFile.Name,
                            Size = listFile.Size,
                            Hash = listFile.Hash
                        };

                        file.Files.Add(new FileNameAndIp
                        {
                            FullName = listFile.FullName,
                            Ip = channelFactories.Endpoint.Address.Uri.Host
                        });

                        list.Add(file);
                    }
                    else
                    {
                        gottenFile.Files.Add(new FileNameAndIp
                        {
                            FullName = listFile.FullName,
                            Ip = channelFactories.Endpoint.Address.Uri.Host
                        });
                    }
                }
            }

            return list;
        } 
    }
}