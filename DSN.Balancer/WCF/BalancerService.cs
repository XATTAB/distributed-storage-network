﻿using System.Collections.Generic;
using DSN.Contracts;
using DSN.Library;

namespace DSN.Balancer.WCF
{
    class BalancerService : IContractBalancer
    {
        private static readonly List<FileInfo> ListFileInfos;

        static BalancerService()
        {
            ListFileInfos = new List<FileInfo>();
        }

        /// <summary>
        /// Добавыить файлы для возрата их балансировщиком.
        /// </summary>
        /// <param name="fileInfo">Список файлов с информацией о серверах.</param>
        public static void AddFile(List<FileInfo> fileInfo)
        {
            ListFileInfos.Clear();
            ListFileInfos.AddRange(fileInfo);
        }

        /// <summary>
        /// Получить все доступные файлы с балансировщика.
        /// </summary>
        /// <returns>Список файлов и информацией о серверах.</returns>
        public List<FileInfo> GetListFiles()
        {
            return ListFileInfos;
        }
    }
}
