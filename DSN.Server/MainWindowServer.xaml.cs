﻿using System.Windows;

namespace DSN.Server
{
    public partial class MainWindowServer
    {
        private readonly WCF.Server _server;
        

        public MainWindowServer()
        {
            InitializeComponent();

            _server = new WCF.Server();
        }

        private void buttonStart_Click(object sender, RoutedEventArgs e)
        {
            if (ButtonStart.Content.ToString() == "Старт")
            {
                TextBox.AppendText(_server.Start());

                ButtonStart.Content = "Стоп";
            }
            else
            {
                TextBox.AppendText(_server.Stop());

                ButtonStart.Content = "Старт";
            }
        }
    }
}
