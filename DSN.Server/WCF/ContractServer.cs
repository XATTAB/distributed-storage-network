﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management;
using System.Text;
using DSN.Contracts;
using DSN.Library;
using FileInfo = DSN.Library.FileInfo;

namespace DSN.Server.WCF
{
    public class ContractServer : IContractServer
    {
        private static readonly ManagementObjectSearcher RamMonitor =    //запрос к WMI для получения памяти ПК
        new ManagementObjectSearcher("SELECT TotalVisibleMemorySize,FreePhysicalMemory FROM Win32_OperatingSystem");

        private static readonly ManagementObjectSearcher CpuMonitor =    //запрос к WMI для получения загружености ЦП
        new ManagementObjectSearcher("SELECT LoadPercentage  FROM Win32_Processor");

        public ServerState GetState()
        {
            var stateServer = new ServerState();

            foreach (var objram in RamMonitor.Get())
            {
                ulong totalRam = Convert.ToUInt64(objram["TotalVisibleMemorySize"]);    //общая память ОЗУ
                ulong busyRam = totalRam - Convert.ToUInt64(objram["FreePhysicalMemory"]);         //занятная память = (total-free)
                stateServer.StateRam = Convert.ToString((busyRam * 100) / totalRam);       //вычисляем проценты занятой памяти
            }

            foreach (var objcpu in CpuMonitor.Get())
            {
                stateServer.StateCpu = Convert.ToString(objcpu["LoadPercentage"]); //записываем проценты загружености ЦП
            }

            return stateServer;
        }

        public List<FileInfo> GetListFiles()
        {
            var files = Directory.GetFiles(Directory.GetCurrentDirectory() + @"\Files");

            return (from file in files
                let fileInfo = new System.IO.FileInfo(file)
                select new FileInfo
                {
                    Name = fileInfo.Name, FullName = file, Size = fileInfo.Length, Hash = CustomerMd5.ComputeMd5Checksum(file)
                }).ToList();
        }

        public IEnumerable<byte> GetBytesArray(string file, long position, long size)
        {
            if (size > Int32.MaxValue)
                throw new ArgumentException("Превышена максимальная величина передаваемых битов!");

            var retBuffer = new List<byte>();

            using (FileStream fs = new FileStream(file, FileMode.Open, FileAccess.Read))
            {
                byte[] buffer = new byte[size];
                using (BinaryReader br = new BinaryReader(fs, new ASCIIEncoding()))
                {
                    fs.Position = position;
                    br.Read(buffer, 0, (int)size);
                }

                retBuffer.AddRange(buffer);

                //for (long i = 0; i < size; i++)
                //{
                //    retBuffer.Add(buffer[i]);
                //}
            }


            return retBuffer;
        }
    }
}
