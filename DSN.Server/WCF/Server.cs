﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using DSN.Contracts;

namespace DSN.Server.WCF
{
    public class Server
    {
        readonly Uri _address;
        readonly BasicHttpBinding _binding;
        ServiceHost _service;
        private ServiceMetadataBehavior behavior;

        public Server()
        {
            _address = new Uri("http://localhost:4000/ServiceGetState");
            _binding = new BasicHttpBinding();
            behavior = new ServiceMetadataBehavior();
        }

        public string Start()
        {
            try
            {
                if (_service == null)
                {
                    _binding.MessageEncoding = WSMessageEncoding.Mtom;
                    _binding.MaxReceivedMessageSize = Int32.MaxValue;
                    _binding.TransferMode = TransferMode.Streamed;
                    _binding.CloseTimeout = new TimeSpan(0, 10, 0);
                    _binding.MaxBufferSize = Int32.MaxValue;
                    _binding.ReceiveTimeout = new TimeSpan(0, 2, 0);
                    _binding.SendTimeout = new TimeSpan(0, 2, 0);

                    _service = new ServiceHost(typeof(ContractServer), _address);
                    _service.AddServiceEndpoint(typeof(IContractServer), _binding, "");


                    behavior.HttpGetEnabled = true;

                    _service.Description.Behaviors.Add(behavior);
                    _service.AddServiceEndpoint(typeof(IMetadataExchange),
                        MetadataExchangeBindings.CreateMexHttpBinding(), _address + "mex");

                    _service.Open();

                    return $"Сервер запущен: {DateTime.Now + Environment.NewLine}";
                }
                return "Сервер уже запущен." + Environment.NewLine;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string Stop()
        {
            try
            {
                if (_service != null)
                {
                    _service.Close();
                    _service = null;
                    return $"Сервер остановлен: {DateTime.Now + Environment.NewLine}";
                }
                return "Сервер уже остановлен.";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
